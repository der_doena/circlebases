package algo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import Model.Edge;
import Model.Graph;
import Model.Matrix;
import Model.Node;

public class Dijkstra<T extends Number> {

	private final Integer ZERO_DISTANCE = 0;

	// Optimal map size without rehashing
	private int optimalMapSize;
	private boolean directed = false;

	private Set<Node<T>> settledNodes;
	private Set<Node<T>> unsettledNodes;
	private Map<Node<T>, Node<T>> predecessors;
	private Map<Node<T>, T> distance;
	private ArrayList<Edge<T>> edgeList;

	private Matrix<T> distanceMatrix;
	private Matrix<LinkedList<Node<T>>> shortestPathMatrix;

	/**
	 * Calculates shortest path between all nodes
	 * 
	 * @param graph
	 *            Graph on which paths should be calculated
	 * @param directed
	 *            True if the algorithm should be calculated on a directed graph
	 */
	public Dijkstra(Graph<T> graph, boolean directed) {
		// Initialize variables;
		optimalMapSize = (int) Math.ceil(graph.getNodes().size() / 0.75 + 1);
		this.directed = directed;
		this.distance = new HashMap<Node<T>, T>(optimalMapSize);
		this.predecessors = new HashMap<Node<T>, Node<T>>(optimalMapSize);
		this.settledNodes = new HashSet<Node<T>>(optimalMapSize);
		this.unsettledNodes = new HashSet<Node<T>>(optimalMapSize);

		int nodeCount = graph.getNodes().size();
		this.distanceMatrix = new Matrix<>(nodeCount, nodeCount);
		this.shortestPathMatrix = new Matrix<>(nodeCount, nodeCount);

		int n = 0;
		for (Node<T> currentNode : graph.getNodes()) {
			runDijkstra(currentNode);
			for (int m = 0; m < nodeCount; m++) {
				this.distanceMatrix.set(n, m, distance.get(graph.getNodes().get(m)));
				this.shortestPathMatrix.set(n, m, this.getPath(graph.getNodes().get(m)));
			}
			n++;
		}
	}

	/**
	 * Calculates shortest path from Startnode to all other nodes
	 * 
	 * @param graph
	 *            Graph on which paths should be calculated
	 * @param node
	 *            Startnode
	 * @param directed
	 *            True if the algorithm should be calculated on a directed graph
	 */
	public Dijkstra(Graph<T> graph, Node<T> node, boolean directed) {

		// Initialize variables;
		optimalMapSize = (int) Math.ceil(graph.getNodes().size() / 0.75 + 1);
		this.directed = directed;
		this.distance = new HashMap<Node<T>, T>(optimalMapSize);
		this.predecessors = new HashMap<Node<T>, Node<T>>(optimalMapSize);
		this.settledNodes = new HashSet<Node<T>>(optimalMapSize);
		this.unsettledNodes = new HashSet<Node<T>>(optimalMapSize);
		runDijkstra(node);
	}

	private void runDijkstra(Node<T> start) {
		this.distance.clear();
		this.predecessors.clear();
		this.settledNodes.clear();
		this.unsettledNodes.clear();

		distance.put(start, (T) ZERO_DISTANCE);
		unsettledNodes.add(start);

		// Repeat until no more unsettled nodes are available
		while (unsettledNodes.size() > 0) {
			// get node from unsettledNodes with minimal entry in distances
			Node<T> node = getMinimum(unsettledNodes);

			// As node has minimum distance, move to settled nodes and remove
			// from unsettled nodes
			settledNodes.add(node);
			unsettledNodes.remove(node);

			if (this.directed) {
				this.edgeList = node.getOutgoingEdges();
			} else {
				this.edgeList = node.getUndirectedEdges();
			}
			findMinimalDistances(node);
		}
	}

	private void findMinimalDistances(Node<T> currentNode) {
		for (Edge<T> edge : this.edgeList) {
			addMinimalDistance(edge);
		}
	}

	/**
	 * 1) Checks if node is already settled --> skip 2) If not settled, check,
	 * if node is already visited --> if not, assign distance as sum of current
	 * node and weight and add node to unsettled list 3) if already visited,
	 * check if distance is shorter to start node, than previous distance -->
	 * update distance and change node predecessor
	 * 
	 * @param edge
	 *            edge between current node and neighbour node
	 */
	private void addMinimalDistance(Edge<T> edge) {

		Node<T> node = edge.getDestination();
		// 1)
		if (!settledNodes.contains(node)) {
			// 2)
			T newDistance = Maths.ObjectMaths.sum(distance.get(edge.getOrigin()), edge.getWeight());
			if (!distance.containsKey(node)) {
				distance.put(node, newDistance);
				unsettledNodes.add(node);
				predecessors.put(node, edge.getOrigin());
			} else {
				// 3)
				if (Maths.ObjectMaths.greaterThan(distance.get(edge.getOrigin()), newDistance)) {
					distance.put(node, newDistance);
					predecessors.put(node, edge.getOrigin());
				}
			}
		}
	}

	/**
	 * Returns the minimal distance of a set of nodes
	 * 
	 * @param nodes
	 *            Set of nodes for which the minimal distance is to be
	 *            calculated
	 * @return Node corresponding to minimal distance
	 */

	private Node<T> getMinimum(Set<Node<T>> nodes) {
		Node<T> minimum = null;
		for (Node<T> node : nodes) {
			if (minimum == null) {
				minimum = node;
			} else {
				if (shorterDistance(minimum, node)) {
					minimum = node;
				}
			}
		}
		return minimum;
	}

	/**
	 * Returns node with shorter distance between the current minimum and an
	 * other node
	 * 
	 * @param currentMin
	 *            Current node with minimal distance to start node
	 * @param secondNode
	 *            Node to check if distance is shorter
	 * @return true if distance of other node is shorter than current node. Else
	 *         otherwise
	 */
	private boolean shorterDistance(Node<T> currentMin, Node<T> secondNode) {
		T d = this.distance.get(secondNode);
		if (d == null) {
			return false; // second node has an infinite distance (not visited
							// yet)
		} else {
			return Maths.ObjectMaths.greaterThan(distance.get(currentMin), distance.get(secondNode));
			// true if current minimal distance is smaller than distance to
			// second node
		}
	}

	/**
	 * Get path from start node to target
	 * 
	 * @param target
	 *            Node to which the path has to be returned
	 * @return LinkedList of nodes from start to path;
	 */
	public LinkedList<Node<T>> getPath(Node<T> target) {
		LinkedList<Node<T>> path = new LinkedList<>();
		Node<T> predecessor = predecessors.get(target);
		if (predecessor == null) {
			return null;
		}
		while (predecessor != null) {
			path.add(predecessor);
			predecessor = predecessors.get(predecessor);
		}

		Collections.reverse(path);
		path.add(target);
		return path;
	}
	
	public Matrix<T> getDistanceMatrix() {
		if (this.distanceMatrix!=null) {
			return this.distanceMatrix;
		} else {
			return(null);
		}
	}
	
	public Matrix<LinkedList<Node<T>>> getShortestPathMatrix() {
		if (this.shortestPathMatrix!=null) {
		return this.shortestPathMatrix;
		} else {
			return null;
		}
	}
}
