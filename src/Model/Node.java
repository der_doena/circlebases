package Model;

import java.util.ArrayList;

public class Node<T> {
	private int id;
	private ArrayList<Edge<T>> outgoingEdges, incomingEdges;

	/**
	 * @param name
	 *            - name of this Node
	 * @param hashSet
	 *            - adjacent Nodes of this Node
	 */
	public Node(int id, ArrayList<Edge<T>> outgoingEdges, ArrayList<Edge<T>> incomingEdges) {
		this.id = id;
		this.outgoingEdges = outgoingEdges;
		this.incomingEdges = incomingEdges;
	}

	/**
	 * @param name
	 *            - name of this Node
	 * @param hashSet
	 *            - adjacent Nodes of this Node
	 */
	public Node(int id) {
		this.id = id;
		this.outgoingEdges = new ArrayList<Edge<T>>();
		this.incomingEdges = new ArrayList<Edge<T>>();
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * @param id
	 *            the name to set
	 */
	public void setID(int id) {
		this.id = id;
	}

	/**
	 * @return the outgoingEdges
	 */
	public ArrayList<Edge<T>> getOutgoingEdges() {
		return outgoingEdges;
	}

	/**
	 * @param outgoingEdges
	 *            the outgoingEdges to set
	 */
	public void setOutgoingEdges(ArrayList<Edge<T>> outgoingEdges) {
		this.outgoingEdges = outgoingEdges;
	}

	/**
	 * adds a edge to the outgoingnodes collection
	 * 
	 * @param edge
	 *            the edge to add
	 */
	public void addOutgoingEdge(Edge<T> edge) {
		this.outgoingEdges.add(edge);
	}

	/**
	 * @return the incomingEdges
	 */
	public ArrayList<Edge<T>> getIncomingEdges() {
		return incomingEdges;
	}

	/**
	 * @param incomingEdges
	 *            the incomingEdges to set
	 */
	public void setIncomingEdges(ArrayList<Edge<T>> incomingEdges) {
		this.incomingEdges = incomingEdges;
	}

	/**
	 * adds a edge to the incoming collection
	 * 
	 * @param edge
	 *            the edge to add
	 */
	public void addIncomingEdge(Edge<T> edge) {
		this.incomingEdges.add(edge);
	}
	
	
	/**
	 * Returns all incoming and outgoing edges as outgoing edges (for example to use in Dijkstra-algorithm)
	 * @return
	 */
	public ArrayList<Edge<T>> getUndirectedEdges(){
		ArrayList<Edge<T>> outgoing=new ArrayList<>();
		for (Edge<T> edge : this.getOutgoingEdges()) {
			outgoing.add(edge);
		}
		for (Edge<T> edge :this.getIncomingEdges()) {
			outgoing.add(new Edge<T>(this,edge.getOrigin(),edge.getWeight()));
		}
		return outgoing;
	}

	/**
	 * 
	 * @return ArrayList of nodes which are in contact with this
	 */
	public ArrayList<Node<T>> getNeighbours() {
		ArrayList<Node<T>> neighbours = new ArrayList<>();
		for (Edge<T> edge : this.getOutgoingEdges()) {
			neighbours.add(edge.getDestination());
		}
		for (Edge<T> edge : this.getIncomingEdges()) {
			neighbours.add(edge.getOrigin());
		}
		return neighbours;		
	}
	
	public String toString() {
		return "Node: "+this.getId();
	}

}
