package Model;

public class Edge<T> {
	private Node<T> node1, node2;
	private T weight;
	
	/**
	 * @param node1
	 * @param weight - weight/distance/notation of this Edge
	 */
	public Edge(Node<T> node1, Node<T> node2, T weight) {
		this.node1 = node1;
		this.node2 = node2;
		this.weight = weight;
	}

	/**
	 * @return the weight
	 */
	public T getWeight() {
		return weight;
	}

	/**
	 * @param weight the weight to set
	 */
	public void setWeight(T weight) {
		this.weight = weight;
	}

	/**
	 * @return the node1
	 */
	public Node<T> getNode1() {
		return node1;
	}

	/**
	 * @param node1 the node1 to set
	 */
	public void setNode1(Node<T> node1) {
		this.node1 = node1;
	}

	/**
	 * @return the node2
	 */
	public Node<T> getNode2() {
		return node2;
	}

	/**
	 * @param node2 the node2 to set
	 */
	public void setNode2(Node<T> node2) {
		this.node2 = node2;
	}
	
	/**
	 * @return the node1
	 */
	public Node<T> getOrigin() {
		return node1;
	}

	/**
	 * @param node1 the node1 to set
	 */
	public void setOrigin(Node<T> node1) {
		this.node1 = node1;
	}

	/**
	 * @return the node2
	 */
	public Node<T> getDestination() {
		return node2;
	}

	/**
	 * @param node2 the node2 to set
	 */
	public void setDestination(Node<T> node2) {
		this.node2 = node2;
	}
	
	public String toString() {
		return this.node1.getId()+" --> "+this.node2.getId()+": " +this.weight;
	}
	
}
