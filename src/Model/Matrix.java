package Model;

import java.util.ArrayList;

public class Matrix<T> {
	ArrayList<ArrayList<T>> matrix;

	/**
	 * Create new 2 dimensional matrix
	 * 
	 * @param numberOfRows
	 *            Number of rows
	 * @param numberOfColumns
	 *            Number of columns
	 */
	public Matrix(int numberOfRows, int numberOfColumns) {
		this.matrix = new ArrayList<>(numberOfColumns);
		for (int m = 0; m < numberOfColumns; m++) {
			ArrayList<T> row = new ArrayList<>();
			for (int n = 0; n < numberOfRows; n++) {
				row.add(null);
			}
			this.matrix.add(row);
		}
	}

	/**
	 * Add value to matrix
	 * 
	 * @param row
	 *            Row
	 * @param column
	 *            Column
	 * @param value
	 *            Value
	 */
	public void set(int row, int column, T value) {
		this.matrix.get(column).set(row, value);
	}

	/**
	 * Get value from Matrix
	 * 
	 * @param row
	 *            Row
	 * @param column
	 *            Column
	 * @return Value
	 */
	public T get(int row, int column) {
		return this.matrix.get(column).get(row);
	}

	/**
	 * 
	 * @return Number of columns
	 */
	public int getColsCount() {
		return matrix.size();
	}

	/**
	 * 
	 * @return Number of rows
	 */
	public int getRowCount() {
		return matrix.get(0).size();
	}

	/**
	 * Returns a copy of the indicated row from matrix
	 * 
	 * @param row
	 *            Row
	 * @return Arraylist with row value copies
	 */
	public ArrayList<T> getRow(int row) {
		ArrayList<T> vector = new ArrayList<>();
		for (int m = 0; m < this.getColsCount(); m++) {
			vector.add(this.get(row, m));
		}
		return vector;
	}

	/**
	 * Returns a copy of the indicated column from matrix
	 * 
	 * @param column
	 *            Column
	 * @return ArrayList with column value copies
	 */
	public ArrayList<T> getColumn(int column) {
		ArrayList<T> newList = new ArrayList<>(this.matrix.get(column));
		return newList;
	}

	/**
	 * Appends vector as row to matrix. Fills empty spaces with null if vector
	 * is shorter or longer than number of columns
	 * 
	 * @param vector
	 *            Row vector as ArrayList<T>
	 */
	public void addAsRow(ArrayList<T> vector) {
		// vector size is smaller than number of coulumns
		if (vector.size() <= this.getColsCount()) {
			for (int i = 0; i < vector.size(); i++) {
				matrix.get(i).add(vector.get(i));
			}
			for (int i = vector.size() - 1; i < this.getColsCount(); i++) {
				matrix.get(i).add(null);
			}
		} else {
			// vector has more elements than columns in matrix
			for (int i = 0; i < vector.size() - this.getColsCount(); i++) {
				ArrayList<T> columns = new ArrayList<>();
				for (int n = 0; n < this.getRowCount(); n++) {
					columns.add(null);
				}
				this.matrix.add(columns);
			}
			for (int i = 0; i < vector.size(); i++) {
				matrix.get(i).add(vector.get(i));
			}
		}
	}

	/**
	 * Appends vector as column to matrix. Fills empty spaces with null if
	 * vector is shorter or longer than number of columns
	 * 
	 * @param vector
	 *            Column vector
	 */
	public void addAsColumn(ArrayList<T> vector) {
		// number of rows is smaller than vector length
		if (vector.size() > this.getRowCount()) {
			for (int i = this.getRowCount(); i < vector.size(); i++) {
				for (int j = 0; j < this.getColsCount(); j++) {
					this.matrix.get(j).add(null);
				}
			}
			matrix.add(vector);
		} else {
			// number of rows is bigger than vector length
			this.matrix.add(vector);
			for (int i = vector.size() - 1; i < this.getRowCount(); i++) {
				this.matrix.get(this.getColsCount() - 1).add(null);
			}
		}
	}

	public String toString() {
		String output = "";
		// Columnlabels
		for (int i = 0; i < this.getColsCount(); i++) {
			output += "\t" + i;
		}
		output += "\n";
		// Rowlabels + data
		for (int n = 0; n < this.getRowCount(); n++) {
			output += n;
			for (int m = 0; m < this.getColsCount(); m++) {
				output += "\t" + this.get(n, m);
			}
			output += "\n";
		}
		return output;
	}

}
