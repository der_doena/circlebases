package Model;

import java.util.ArrayList;

public class Graph<T> {
	private ArrayList<Node<T>> nodes;
	private ArrayList<Edge<T>> edges;
	private boolean isDirected;

	
	/**
	 * @param node
	 * @param edges
	 * This Graph is directed.
	 * for undirected Graph add for every edge A--B add an additional edge B--A
	 */
	public Graph(ArrayList<Node<T>> nodes, ArrayList<Edge<T>> edges, boolean isDirected) {
		this.nodes = nodes;
		this.edges = edges;
		this.isDirected = isDirected;
		for (int i = 0; i < edges.size(); i++) {
			Edge<T> edge = edges.get(i);
			edge.getNode1().addOutgoingEdge(edge);
			edge.getNode2().addIncomingEdge(edge);
		}
	}
	
	/**
	 * generates a Graph containing Nodes with Names: 0 to nodesCount-1 and without Edges
	 * @param nodesCount
	 */
	public Graph(int nodesCount) {
		ArrayList<Node<T>> nodes = new ArrayList<Node<T>>();
		for (int i = 0; i < nodesCount; i++) {
			nodes.add(new Node<T>(i));
		}
		this.nodes = nodes;
		this.edges = new ArrayList<Edge<T>>();
	}
	
	public int[][] toMatrix(){
		int[][] matrix = new int[this.getNodes().size()][this.getNodes().size()];
		//to do
		return matrix;
	}
	
	/**
	 * generates a String listing all Nodes and Edges of this Graph
	 */
	public String toString() {
		String s = "Graph:   ------------------------------------------------\n";
		for (Node<T> node : getNodes()) {
			s += "" + node.getId();
			for (Edge<T> edge : node.getOutgoingEdges()) {
				s += "\t --" + edge.getWeight() + "-->" + edge.getNode2().getId();
			}
			s += "\n";
		}
		s += "nodes count:" + getNodes().size() + "\n";
		s += "edges count:" + getEdges().size() + "\n";
		s += "--------------------------------------------------------\n";
		return s;
	}

	/**
	 * @return the nodes
	 */
	public ArrayList<Node<T>> getNodes() {
		return nodes;
	}

	/**
	 * @param nodes the nodes to set
	 */
	public void setNodes(ArrayList<Node<T>> nodes) {
		this.nodes = nodes;
	}

	/**
	 * @return the edges
	 */
	public ArrayList<Edge<T>> getEdges() {
		return edges;
	}

	/**
	 * @param edges the edges to set
	 */
	public void setEdges(ArrayList<Edge<T>> edges) {
		this.edges = edges;
	}
	
	public void setEdges(Node<T> origin, Node<T> destination, T weight){
		if (origin !=null && destination !=null) {
			edges.add(new Edge<T> (origin, destination, weight));
		} else {
			throw new NullPointerException("One of the nodes is null");
		}
	}
	
	public void setEdges(int origin, int destination, T weight) {
		
		if (origin<this.nodes.size() && destination<this.nodes.size()) {
			Node<T> node1 = this.nodes.get(origin);
			Node<T> node2 = this.nodes.get(destination);
			Edge<T> edge = new Edge<>(node1,node2,weight);
			node1.addOutgoingEdge(edge);
			node2.addIncomingEdge(edge);
			edges.add(edge);			
		}
	}

	/**
	 * @return the isDirected
	 */
	public boolean isDirected() {
		return isDirected;
	}

	/**
	 * @param isDirected the isDirected to set
	 */
	public void setDirected(boolean isDirected) {
		this.isDirected = isDirected;
	}
	

}
