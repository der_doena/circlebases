package UserInterface;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Vector;

import Model.*;

public class Parser {
	private static Parser instance;
	
	private String readDir;
	private BufferedReader in;
	
	/**
	 * @return an instance of Parser
	 */
	public static Parser getInstance() {
		if (instance == null) {
			instance = new Parser();
		}
		return instance;
	}
	
	/**
	 * generates a new Parser object
	 */
	public Parser() {
		this.readDir = "data" + File.separator + "graph.data";
		try {
			in = new BufferedReader(new FileReader(new File(this.readDir)));
		} catch (Exception e) { e.printStackTrace(); }
	}
	
	/**
	 * reads a line of the file, handles exceptions
	 * @return the line
	 */
	public String readLine(){
		try {
			return this.in.readLine();
		} catch (Exception e) { e.printStackTrace(); }
		return null;
	}
	
	/**
	 * parses Graphs out of files
	 * @param <T>
	 * @return the Graphs
	 */
	public <T extends Number> HashSet<Graph<T>> parseGraphs(Class<T> cls){
		String line;
		String args[];
		ArrayList<Node<T>> nodes;
		ArrayList<Edge<T>> edges;
		boolean isDirected;
		HashSet<Graph<T>> graphs;

		graphs = new HashSet<Graph<T>>();
		while (true) {
			nodes = new ArrayList<Node<T>>();
			edges = new ArrayList<Edge<T>>();
			// parse first line of graph definition (nodes)
			line = readLine();
			if (line == null) { break; }
			// skip empty lines
			while (true) {
				if (line.isEmpty() || line.trim().equals("") || line.trim().equals("\n") || line.startsWith("#")) {
					line = readLine();
				} else {
					break;
				}
			}
			for (int i = 0; i < Integer.parseInt(line); i++) {
				nodes.add(new Node<T>(i+1));
			}
			// parse second line of graph definition (edges)
			line = readLine();
			if (line == null) { break; }
			args = line.split(";");
			for (String arg : args) {
				if (cls == Integer.class) {
					edges.add(new Edge<T>(nodes.get(Integer.parseInt(arg.split(",")[0])-1), nodes.get(Integer.parseInt(arg.split(",")[1])-1), cls.cast(Integer.parseInt(arg.split(",")[2]))));
				} else if (cls == Float.class) {
					edges.add(new Edge<T>(nodes.get(Integer.parseInt(arg.split(",")[0])-1), nodes.get(Integer.parseInt(arg.split(",")[1])-1), cls.cast(Float.parseFloat(arg.split(",")[2]))));
				} else if (cls == Double.class) {
					edges.add(new Edge<T>(nodes.get(Integer.parseInt(arg.split(",")[0])-1), nodes.get(Integer.parseInt(arg.split(",")[1])-1), cls.cast(Double.parseDouble(arg.split(",")[2]))));
				} else {
					edges.add(new Edge<T>(nodes.get(Integer.parseInt(arg.split(",")[0])-1), nodes.get(Integer.parseInt(arg.split(",")[1])-1), cls.cast(arg.split(",")[2])));
				}
			}
			// parse last line of graph definition (isDirected)
			line = readLine();
			if (line == null) { break; }
			isDirected = Boolean.parseBoolean(line);
			// create Graph
			graphs.add(new Graph<T>(nodes, edges, isDirected));
		}
		return graphs;
	}
	
	public <E> E minmax(Vector<E> v, boolean mm) {
		return null;	
	}

}
