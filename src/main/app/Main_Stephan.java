package main.app;

import java.util.HashSet;

import Model.Graph;
import UserInterface.Parser;
import algo.Dijkstra;

public class Main_Stephan {
	public static void main(String[] args) {

		// Graph von Hand + Dijkstra:
		Graph<Integer> simplestDijkstraGraph = new Graph<Integer>(6);
		simplestDijkstraGraph.setEdges(0, 1, 2);
		simplestDijkstraGraph.setEdges(0, 2, 10);
		simplestDijkstraGraph.setEdges(1, 2, 2);
		simplestDijkstraGraph.setEdges(2, 3, 2);
		simplestDijkstraGraph.setEdges(3, 4, 7);
		simplestDijkstraGraph.setEdges(4, 5, 10);
		simplestDijkstraGraph.setEdges(5, 3, 1);
		simplestDijkstraGraph.setEdges(5, 2, 1);

		Dijkstra<Integer> dijkstra = new Dijkstra<Integer>(simplestDijkstraGraph, false);
		System.out.println(dijkstra.getDistanceMatrix());
		System.out.println(dijkstra.getShortestPathMatrix());

		//Graph von Parser:
		HashSet<Graph<Integer>> graphs = Parser.getInstance().parseGraphs(Integer.class);
		System.out.println(graphs.size());
		for (Graph<?> graph : graphs) {
			System.out.println(graph);
			System.out.println(graph.getEdges().get(0).getWeight().getClass());
		}
	}
}
