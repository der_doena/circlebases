package Maths;

public class Fp {
	int p;
	
	/**
	 * static modulo method, because % is not working with negative numbers
	 * @param x
	 * @param p
	 * @return
	 */
	private static int mod(int x, int p) {
		int result = x % p;
		if (result < 0) {
			result += p;
		}
		return result;
	}

	
	public Fp(int p) {
		this.p = p;
	}
	
	/**
	 * add method in this Fp
	 * @param el1
	 * @param el2
	 * @return
	 */
	public int add(int el1, int el2) {
		int result = 0;
		if (this.p == 0) {
			result = el1 + el2;
		} else {
			result = Fp.mod((el1 + el2), p);
		}
		return result;
	}
	
	/**
	 * subtract method in this Fp
	 * @param el1
	 * @param el2
	 * @return
	 */
	public int subtract(int el1, int el2) {
		int result = 0;
		if (this.p == 0) {
			result = el1 - el2;
		} else {
			result = Fp.mod((el1 - el2), p);
		}
		return result;
	}

	/**
	 * multiplication method in this Fp
	 * @param el1
	 * @param faktor
	 * @return
	 */
	public int multiplicate(int el1, int factor) {
		int result = 0;
		if (this.p == 0) {
			result = el1 * factor;
		} else {
			result = Fp.mod((el1 * factor), this.p);
		}
		return result;
	}

}
