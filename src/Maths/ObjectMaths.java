package Maths;

/**
 * Class for calculation on Number objects
 * @author Grafen der Graphen (ST)
 *
 */

public class ObjectMaths {
	/**
	 * Returns the sum of two generic Number objects without rounding errors
	 * 
	 * @param a
	 *            First object
	 * @param b
	 *            Second object
	 * @return Returns the sum of both Number objects <T extends Number>
	 */
	public static<T extends Number> T sum(T a, T b) {
		if (a.getClass().getSimpleName().equals("Integer")) {
			Integer result = a.intValue() + b.intValue();
			return (T) result;
		}
		if (a.getClass().getSimpleName().equals("Double")) {
			Double result = a.doubleValue() + b.doubleValue();
			return (T) result;
		}
		return null;
	}

	/**
	 * Compares two generic Number objects without rounding error
	 * 
	 * @param a
	 *            First object
	 * @param b
	 *            Second object
	 * @return True if first object is greater than the second. Returns false
	 *         otherwise. Returns false if objects are either of type Integer
	 *         nor Double.
	 */
	public static <T extends Number>boolean greaterThan(T a, T b) {
		if (a.getClass().getSimpleName().equals("Integer")) {
			if (a.intValue() > b.intValue()) {
				return true;
			} else {
				return false;
			}
		}
		if (a.getClass().getSimpleName().equals("Double")) {
			if (a.doubleValue() > b.doubleValue()) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
	
}
