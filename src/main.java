
import java.util.HashSet;

import Model.*;
import UserInterface.*;

public class main {
	
	public static void main(String[] args) {
		Graph<Integer> g = new Graph<Integer>(10);
		System.out.println(g.toString());
		
		HashSet<Graph<Integer>> graphs = Parser.getInstance().parseGraphs(Integer.class);
		System.out.println(graphs.size());
		for (Graph<?> graph : graphs) {
			System.out.println(graph);
			System.out.println(graph.getEdges().get(0).getWeight().getClass());
		}
	}

}
